package ecs

import "testing"

func TestRegisterComponent(t *testing.T) {
	const (
		GAME = iota
		PLAYER
		ENEMY
	)
	var w World
	tests := []struct {
		id ComponentID
		cs ComponentStorage
	}{{GAME, new(FlagStorage)}, {PLAYER, new(FlagStorage)}, {ENEMY, new(FlagStorage)}}
	for _, test := range tests {
		w.RegisterComponent(Component{test.id, test.cs})
		c := w.Component(test.id)
		if c.ID != test.id {
			t.Errorf("Expected id %v, got %v", test.id, c.ID)
		}
		if c.ComponentStorage != test.cs {
			t.Errorf("Expected %v, got %v", test.cs, c.ComponentStorage)
		}
	}
}
func TestRegisterComponents(t *testing.T) {
	const (
		GAME = iota
		PLAYER
		ENEMY
	)
	var w World
	tests := []Component{
		Component{GAME, new(FlagStorage)},
		Component{PLAYER, new(FlagStorage)},
		Component{ENEMY, new(FlagStorage)},
	}
	w.RegisterComponents(tests...)
	for _, test := range tests {
		c := w.Component(test.ID)
		if c.ID != test.ID {
			t.Errorf("Expected id %v, got %v", test.ID, c.ID)
		}
		if c.ComponentStorage != test.ComponentStorage {
			t.Errorf("Expected %v, got %v", test.ComponentStorage, c.ComponentStorage)
		}
	}
}

func TestEntityManagement(t *testing.T) {
	const (
		PLAYER = iota
		ENEMY
		HEALTH
	)
	type Player struct {
		Score int
		Kills int
	}
	type Health struct {
		Max int
		Now int
	}
	var w World
	w.RegisterComponents(
		Component{PLAYER, new(MapStorage)},
		Component{ENEMY, new(FlagStorage)},
		Component{HEALTH, new(MapStorage)},
	)
	var p, e Entity
	var err error
	if p, err = w.MakeEntity(
		ComponentValue{PLAYER, &Player{10, 2}},
		ComponentValue{HEALTH, &Health{1000, 900}},
	); err != nil {
		t.Errorf("Expected no errors, got %v", err)
	}
	if e, err = w.MakeEntity(
		ComponentValue{ID: ENEMY},
	); err != nil {
		t.Errorf("Expected no errors, got %v", err)
	}
	if err = w.AddComponents(e, ComponentValue{HEALTH, &Health{100, 0}}); err != nil {
		t.Errorf("Expected no errors, got %v", err)
	}

	pv := w.Component(PLAYER).Get(p).(*Player)
	if pv == nil {
		t.Errorf("Expect retrieve player, got none")
	}
	if pv.Score != 10 {
		t.Errorf("Expect Score %v, got %v", 10, pv.Score)
	}
	if pv.Kills != 2 {
		t.Errorf("Expect Kills %v, got %v", 2, pv.Kills)
	}

	ph := w.Component(HEALTH).Get(p).(*Health)
	if ph == nil {
		t.Errorf("Expect retrieve healh, got none")
	}
	if ph.Max != 1000 {
		t.Errorf("Expect Max %v, got %v", 2, ph.Max)
	}
	if ph.Now != 900 {
		t.Errorf("Expect Max %v, got %v", 900, ph.Now)
	}

	eh := w.Component(HEALTH).Get(e).(*Health)
	if eh == nil {
		t.Errorf("Expect retrieve healh, got none")
	}
	if eh.Max != 100 {
		t.Errorf("Expect Max %v, got %v", 100, eh.Max)
	}
	if eh.Now != 0 {
		t.Errorf("Expect Max %v, got %v", 0, eh.Now)
	}

	w.DropComponents(e, ENEMY, HEALTH)
	v := w.Component(HEALTH).Get(e)
	if v != nil {
		t.Errorf("Expect not retrieve healh, got one")
	}

}
