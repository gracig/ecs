# ecs

Element Component System

An ECS Architecture for games

Features:
1) Components are indexed in ordered LLRB Trees for fast retrieval
2) Games are described in CNF  Conjunctive Normal Form
3)