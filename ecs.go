package ecs

import (
	"fmt"
	"log"
	"sync"
	"sync/atomic"
	"time"

	"github.com/petar/GoLLRB/llrb"
)

//Entity represents an instance of any kind.
type Entity uint64

//ComponentID is the ID of a Component Class.
type ComponentID uint32

//Component add behaviour to an Entity
type Component struct {
	//ID identifies this component inside the world
	ID ComponentID
	//ComponentStorage is used to persist component instances.
	//We have so far two ComponentStorage implementation:
	//FlagStorage and MapStorage
	ComponentStorage
}

//Value is a component value representation
type Value interface{}

//ComponentValue is a container to be used when setting values to an entity
type ComponentValue struct {
	ID    ComponentID
	Value Value
}

//ComponentStorage is an interface that enables the developer build its own persistence strategy
type ComponentStorage interface {
	//Init is called when registering components
	Init(w *World, id ComponentID)
	//Get the value of the component of a especific entity.
	//returns nil if no value is found.
	Get(id Entity) Value
	//put insert or update a component value to an entity. used internally
	put(id Entity, comp Value)
	//del Remove the component from the interface. used internally
	del(id Entity) Value
}

//System is the representation of a processing unit
type System struct {
	CNF     CNF
	Rate    time.Duration
	Init    func(w *World)
	OnLoop  func(w *World, elapsed time.Duration) func(x []Entity)
	OnEmpty func(w *World, elapsed time.Duration) (stop bool)
}

//World manages all the entities,components and systems
type World struct {
	//idgen is the entity id sequencial generator
	idgen uint64
	//indexes store an map of components rbtree indexes
	indexes struct {
		sync.Mutex
		atomic.Value
	}
}

//RunSystem receives a System and executes in a goroutine
func (w *World) RunSystem(s System) {
	go func() {
		s.Init(w)
		start := time.Now()
		ticker := time.NewTicker(s.Rate)
		for t := range ticker.C {
			elapsed := time.Since(start)
			count, err := s.CNF.Apply(w, s.OnLoop(w, elapsed))
			if err != nil {
				log.Printf("%v", err)
				continue
			}
			if count == 0 && s.OnEmpty(w, elapsed) {
				return
			}
			start = t
		}
	}()
}

//RegisterComponent creates an index for the inputed Component
func (w *World) RegisterComponent(c Component) {
	w.indexes.Lock()
	defer w.indexes.Unlock()
	idxnew := make(map[ComponentID]cidx)
	idxold := w.indexes.Load()
	if idxold != nil {
		for k, v := range idxold.(map[ComponentID]cidx) {
			idxnew[ComponentID(k)] = v
		}
	}
	idxnew[c.ID] = cidx{c, llrb.New()}
	w.indexes.Store(idxnew)
	c.Init(w, c.ID)
}

//RegisterComponents creates an index for every inputed components
func (w *World) RegisterComponents(cs ...Component) {
	for _, c := range cs {
		w.RegisterComponent(c)
	}
}

//Components retrieves all registered components
func (w *World) Components() (cs []Component) {
	for _, v := range w.indexes.Load().(map[ComponentID]cidx) {
		cs = append(cs, v.Component)
	}
	return
}

//Component retrieves a component by its id
func (w *World) Component(id ComponentID) Component {
	return w.indexes.Load().(map[ComponentID]cidx)[id].Component
}

//ComponentInterval returns the min/max Entity interval of a component
func (w *World) ComponentInterval(id ComponentID) (min, max Entity) {
	idx := w.indexes.Load().(map[ComponentID]cidx)[id]
	if idx.Len() <= 0 {
		min, max = 0, 0
		return
	}
	min, max = idx.Min().(Entity), idx.Max().(Entity)
	return
}

//Interval returns the min/max Entity interval of the entire world
func (w *World) Interval() (min, max Entity) {
	return 1, Entity(w.idgen)
}

//ComponentAscendInterval iterate over entities in ascendant order from min to max
func (w *World) ComponentAscendInterval(id ComponentID, min, max Entity, fn func(e Entity) bool) {
	idx := w.indexes.Load().(map[ComponentID]cidx)[id]
	idx.AscendRange(min, max+1, func(item llrb.Item) bool {
		return fn(item.(Entity))
	})
}

//MakeEntity creates a new Entity and add a bunch of components to it
func (w *World) MakeEntity(values ...ComponentValue) (Entity, error) {
	e := Entity(atomic.AddUint64(&w.idgen, 1))
	if err := w.AddComponents(e, values...); err != nil {
		return 0, err
	}
	return e, nil
}

//AddComponents add a bunch of component values to an entity
func (w *World) AddComponents(e Entity, values ...ComponentValue) error {
	cidx := w.indexes.Load().(map[ComponentID]cidx)
	for _, v := range values {
		idx, ok := cidx[v.ID]
		if !ok {
			return fmt.Errorf("component %v not registered", v.ID)
		}
		idx.ReplaceOrInsert(e) //Index
		idx.put(e, v.Value)    //Storage
	}
	return nil
}

//DropComponents drops a bunch of component values from an entity
func (w *World) DropComponents(e Entity, cids ...ComponentID) {
	cidx := w.indexes.Load().(map[ComponentID]cidx)
	for _, cid := range cids {
		idx, ok := cidx[cid]
		if !ok {
			continue
		}
		idx.Delete(e) //Index
		idx.del(e)    //Storage
	}
	return
}

type cidx struct {
	Component
	*llrb.LLRB
}

func (ci cidx) Less(than llrb.Item) bool {
	return ci.Len() < than.(cidx).Len()
}

//Less returns true if this entity is less than another entity
func (e Entity) Less(than llrb.Item) bool {
	return e < than.(Entity)
}

//MapStorage is a type to store component values internally in a native golang map
type MapStorage struct {
	M  map[Entity]Value
	ID ComponentID
	sync.RWMutex
}

//Init initializes the map storage
func (v *MapStorage) Init(w *World, id ComponentID) {
	v.ID = id
	v.M = make(map[Entity]Value)
}

//Get returns the value of a specific entity
func (v *MapStorage) Get(id Entity) Value {
	v.RLock()
	defer v.RUnlock()
	c, ok := v.M[id]
	if !ok {
		return nil
	}
	return c
}
func (v *MapStorage) put(id Entity, c Value) {
	v.Lock()
	defer v.Unlock()
	v.M[id] = c
}
func (v *MapStorage) del(id Entity) Value {
	v.Lock()
	defer v.Unlock()
	c, ok := v.M[id]
	if !ok {
		return nil
	}
	delete(v.M, id)
	return c
}

//FlagStorage is a special kind of storage that does not persist any thing but
//can return true or false wether a component is or not associated to an entity
type FlagStorage struct {
	idx cidx
}

//Init initializes the map storage
func (v *FlagStorage) Init(w *World, id ComponentID) {
	v.idx = w.indexes.Load().(map[ComponentID]cidx)[id]
}

//Get returns true if a component exists for an entity
func (v *FlagStorage) Get(id Entity) Value {
	return v.idx.Has(id)
}
func (v *FlagStorage) put(id Entity, value Value) {
	//No need to store anything here. just the index
}
func (v *FlagStorage) del(id Entity) Value {
	//No need to delete anything here. idx will manage that
	return true
}
