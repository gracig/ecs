package ecs

import (
	"fmt"

	"github.com/petar/GoLLRB/llrb"
)

//X represents a variable in the Conjunctive Normal Form
type X uint8

//Literal represents a literal in the Conjunctive Normal Form
type Literal struct {
	xn    X                                   //The x n-ary
	kind  int                                 //The kind of the literal
	not   bool                                //Negates the literal
	cid   ComponentID                         //The componentID for tHas use
	xs    []X                                 //The function n-arity
	cnf   CNF                                 //A cnf Expression for tCNF use
	ps    []Projection                        //Projection for PFunction use
	pfn   func(es []Entity, pvs []Value) bool //A Projection Function. Returns false to filter out
	onVar X                                   //A X variable to project
}

//Clause representes the Clause in na Conjunctive Normal Form.
//It is, is essence, a list of Literal
type Clause []Literal

//CNF is a Conjunctive Normal Form expression. It is a list of Clauses
type CNF []Clause

//NFBuilder hekps to build CNF Expressions
type NFBuilder struct{}

//CNF creates a CNF expression upon the received Clauses
func (b NFBuilder) CNF(cs ...Clause) CNF {
	return CNF(cs)
}

//Clause creates a Clase upon the received Literals
func (b NFBuilder) Clause(lts ...Literal) Clause {
	return Clause(lts)
}

//Not negates a Literal
func (b NFBuilder) Not(l Literal) Literal {
	l.not = true
	return l
}

//Has creates a Literal that check if an Entity has a specific Component
func (b NFBuilder) Has(xn X, cid ComponentID) Literal {
	return Literal{
		xn:   xn,
		kind: tHas,
		cid:  cid,
	}
}

//CNFPred creates a literal based on a CNF expression
func (b NFBuilder) CNFPred(xn X, cnf CNF) Literal {
	return Literal{
		xn:   xn,
		kind: tCNF,
		cnf:  cnf,
	}
}

//Product creates a Literal that is a function to produce the cartesian product
//of every variable X in ascendent order from Left to Right
func (b NFBuilder) Product(xn X, xs ...X) Literal {
	return Literal{
		xn:   xn,
		kind: tProduct,
		xs:   xs,
	}
}

//PFunction filter entities after have applied some logic to them
func (b NFBuilder) PFunction(xn X, ps []Projection, onVar X,
	pfn func(es []Entity, pvs []Value) bool) Literal {
	return Literal{
		xn:    xn,
		kind:  tPFunction,
		ps:    ps,
		pfn:   pfn,
		onVar: onVar,
	}
}

//Apply executes function fn for any tuple of variables found after CNF evaluation
func (c CNF) Apply(w *World, fn func(x []Entity)) (int, error) {
	var count int
	plan, min, max, err := resolveDomain(w, c)
	_ = plan
	if err != nil {
		return count, err
	}
	if len(min) == 0 {
		return count, nil
	}
	for i := range min {
		if max[i].Less(min[i]) {
			return count, nil
		}
	}
	for entities := range resolveCNF(w, plan, min, max) {
		count++
		fn(entities)
	}
	return count, nil
}

//Domain estimates the min and max Entities for this CNF
func (c CNF) Domain(w *World) (min, max Entity, err error) {
	_, xmin, xmax, e := resolveDomain(w, c)
	if e != nil {
		err = e
		return
	}
	if len(xmin) == 0 {
		return
	}
	for i := range xmin {
		if i == 0 {
			min, max = xmin[i], xmax[i]
			continue
		}
		if xmin[i].Less(min) {
			min = xmin[i]
		}
		if max.Less(xmax[i]) {
			max = xmax[i]
		}
	}
	return
}

//Fetch returns the entities found indexed by row and CNF X Variable
func (c CNF) Fetch(w *World) (entities [][]Entity, err error) {
	_, err = c.Apply(w, func(x []Entity) {
		entities = appendEntitySlice(entities, x)
	})
	return
}

//Projection is a way to select which component value to pick
//from the eneity index resulted from a CNF evaluation
type Projection struct {
	Idx int
	Cid ComponentID
}

//ApplyProjection evaluates fn where each value in v []Value is a
//direct representation of a projeciton in ps []Projection
//nil values should be deleted from their indexes after fn evaluation
func (c CNF) ApplyProjection(w *World,
	ps []Projection, fn func(v []Value)) (count int, err error) {
	vs := make([]Value, len(ps))
	count, err = c.Apply(w, func(es []Entity) {
		for i, p := range ps {
			vs[i] = w.Component(p.Cid).Get(es[p.Idx])
		}
		fn(vs)
		for i, v := range vs {
			if v == nil {
				w.DropComponents(es[ps[i].Idx], ps[i].Cid)
			}
		}
	})
	return
}

//FetchProjection returns the values found indexed by row and projection index
func (c CNF) FetchProjection(w *World,
	ps []Projection) (values [][]Value, err error) {
	_, err = c.ApplyProjection(w, ps, func(vs []Value) {
		values = appendValueSlice(values, vs)
	})
	return
}

const (
	pPred = iota
	pNotPred
	pFunc
	pNotFunc
)
const (
	tHas = iota
	tCNF
	tProduct
	tPFunction
)

type eb struct {
	e []Entity
}

func (e eb) Less(than eb) bool {
	for i, e := range e.e {
		if !(e < than.e[i]) {
			return false
		}
	}
	return true
}

func resolveDomain(w *World, cnf CNF) (plan [][]CNF, min, max []Entity, err error) {
	mplan := make(map[int]map[X]CNF)
	for i, c := range cnf {
		mplan[i] = make(map[X]CNF)
		cmin := make(map[X]Entity)
		cmax := make(map[X]Entity)
		for _, l := range c {
			//Make plan for variable l.xn
			if len(mplan[i][l.xn]) == 0 {
				mplan[i][l.xn] = make(CNF, 4)
			}
			//Instantiate min and max with full Entity range for variable l.xn
			for len(min) < int(l.xn)+1 {
				min = append(min, Entity(1))
				max = append(max, Entity(w.idgen))
			}
			//Find min/max and build plan for each type of literal
			switch l.kind {
			case tHas:
				if l.not {
					mplan[i][l.xn][pNotPred] = append(mplan[i][l.xn][pNotPred], l)
					continue
				}
				mplan[i][l.xn][pPred] = append(mplan[i][l.xn][pPred], l)
				idx, ok := w.indexes.Load().(map[ComponentID]cidx)[l.cid]
				if !ok {
					err = fmt.Errorf("component [%v] not registered", l.cid)
					return
				}
				if idx.Len() <= 0 {
					cmin[l.xn] = Entity(1)
					cmax[l.xn] = Entity(0)
					continue
				}
				if _, ok := cmin[l.xn]; !ok {
					cmin[l.xn] = idx.Min().(Entity)
					cmax[l.xn] = idx.Max().(Entity)
				} else {
					if cmax[l.xn].Less(idx.Max()) {
						cmax[l.xn] = idx.Max().(Entity)
					}
					if idx.Min().Less(cmin[l.xn]) {
						cmin[l.xn] = idx.Min().(Entity)
					}
				}
			case tCNF:
				if l.not {
					mplan[i][l.xn][pNotPred] = append(mplan[i][l.xn][pNotPred], l)
					continue
				}
				mplan[i][l.xn][pPred] = append(mplan[i][l.xn][pPred], l)
				cnfmin, cnfmax, e := l.cnf.Domain(w)
				if e != nil {
					err = e
					return
				}
				if _, ok := cmin[l.xn]; !ok {
					cmin[l.xn] = cnfmin
					cmax[l.xn] = cnfmax
				} else {
					if cmax[l.xn].Less(cnfmax) {
						cmax[l.xn] = cnfmax
					}
					if cnfmin.Less(cmin[l.xn]) {
						cmin[l.xn] = cnfmin
					}
				}
			default:
				if l.not {
					mplan[i][l.xn][pNotFunc] = append(mplan[i][l.xn][pNotFunc], l)
					continue
				}
				mplan[i][l.xn][pFunc] = append(mplan[i][l.xn][pFunc], l)
			}
		}
		for x := range cmin {
			if min[x].Less(cmin[x]) {
				min[x] = cmin[x]
			}
			if cmax[x].Less(max[x]) {
				max[x] = cmax[x]
			}
		}
	}
	for c := range mplan {
		for x, p := range mplan[c] {
			for len(plan) < int(x)+1 {
				plan = append(plan, make([]CNF, 0, 3))
			}
			for i, l := range p {
				if l == nil {
					continue
				}
				for len(plan[int(x)]) < i+1 {
					plan[int(x)] = append(plan[int(x)], CNF{})
				}
				plan[int(x)][i] = append(plan[int(x)][i], l)
			}
		}
	}
	return
}
func resolveCNF(w *World, plan [][]CNF, min, max []Entity) chan []Entity {
	out := make(chan []Entity, 1)
	go func() {
		defer close(out)
		xn := X(len(min) - 1)
		lvl := len(plan[int(xn)]) - 1
		for x := range resolveVariable(w, plan, min, max, xn, lvl) {
			out <- x.e
		}
	}()
	return out
}
func resolveVariable(w *World, plan [][]CNF, min, max []Entity, xn X, lvl int) chan eb {
	out := make(chan eb, 1)
	go func() {
		defer close(out)
		clauseIdx := len(plan[int(xn)][lvl]) - 1
		switch clauseIdx {
		case -1:
			//Generate all elements if nil and pPred
			if lvl == pPred {
				for i := 1; i < int(w.idgen+1); i++ {
					out <- eb{[]Entity{Entity(i)}}
				}
			}
		default:
			for xc := range resolveClause(w, plan, clauseIdx, min, max, xn, lvl) {
				out <- xc
			}
		}
	}()
	return out
}
func resolveClause(w *World, plan [][]CNF, idx int, min, max []Entity, xn X, lvl int) chan eb {
	clause := plan[int(xn)][lvl][idx]
	out := make(chan eb, 1)
	go func() {
		defer close(out)
		fnRange := func(out chan eb) {
			litIdx := len(clause) - 1
			for ebs := range resolveLiteral(w, plan, clause, litIdx, min, max, xn, lvl) {
				out <- ebs
			}
		}
		switch idx {
		case 0:
			fnRange(out)
		default:
			out1 := make(chan eb, 1)
			out2 := resolveClause(w, plan, idx-1, min, max, xn, lvl)
			go func() {
				defer close(out1)
				fnRange(out1)
			}()
			chAnd(out, out1, out2)
		}
	}()
	return out
}

func resolveLiteral(w *World, plan [][]CNF, ls []Literal, idx int, min, max []Entity, xn X, lvl int) chan eb {
	out := make(chan eb, 1)
	lit := ls[idx]
	go func() {
		defer close(out)
		switch idx {
		case 0:
			eval(w, plan, lit, min, max, xn, lvl, out)
		default:
			out1 := make(chan eb, 1)
			out2 := resolveLiteral(w, plan, ls, idx-1, min, max, xn, lvl)
			go func() {
				defer close(out1)
				eval(w, plan, lit, min, max, xn, lvl, out1)
			}()
			chOr(out, out1, out2)
		}
	}()
	return out
}
func eval(w *World, plan [][]CNF, l Literal,
	min, max []Entity, xn X, lvl int, out chan eb) {
	switch lvl {
	case pPred:
		switch l.kind {
		case tHas:
			evalHas(w, plan, l, min, max, xn, lvl, out)
		case tCNF:
			evalCNF(w, plan, l, min, max, xn, lvl, out)
		}
	case pNotPred:
		switch l.kind {
		case tHas:
			evalNotHas(w, plan, l, min, max, xn, lvl, out)
		case tCNF:
			evalNotCNF(w, plan, l, min, max, xn, lvl, out)
		}
	case pFunc:
		switch l.kind {
		case tProduct:
			evalProduct(w, plan, l, min, max, xn, lvl, out)
		case tPFunction:
			evalPFunction(w, plan, l, min, max, xn, lvl, out)
		}
	case pNotFunc:
	default:

	}
}
func evalCNF(w *World, plan [][]CNF, l Literal,
	min, max []Entity, xn X, lvl int, out chan eb) {
	l.cnf.Apply(w, func(x []Entity) {
		out <- eb{x}
	})
}
func evalNotCNF(w *World, plan [][]CNF, l Literal,
	min, max []Entity, xn X, lvl int, out chan eb) {
	yes := resolveVariable(w, plan, min, max, xn, lvl-1)
	not := make(chan eb, 1)
	go func() {
		defer close(not)
		l.cnf.Apply(w, func(x []Entity) {
			not <- eb{x}
		})
	}()
	chNot(out, yes, not)
}
func evalHas(w *World, plan [][]CNF, l Literal,
	min, max []Entity, xn X, lvl int, out chan eb) {
	x := int(xn)
	idx := w.indexes.Load().(map[ComponentID]cidx)[l.cid]
	idx.AscendRange(min[x], max[x]+1, func(id llrb.Item) bool {
		out <- eb{[]Entity{id.(Entity)}}
		return true
	})
}
func evalNotHas(w *World, plan [][]CNF, l Literal,
	min, max []Entity, xn X, lvl int, out chan eb) {
	x := int(xn)
	yes := resolveVariable(w, plan, min, max, xn, lvl-1)
	not := make(chan eb, 1)
	go func() {
		defer close(not)
		idx := w.indexes.Load().(map[ComponentID]cidx)[l.cid]
		idx.AscendRange(min[x], max[x]+1, func(id llrb.Item) bool {
			not <- eb{[]Entity{id.(Entity)}}
			return true
		})
	}()
	chNot(out, yes, not)
}
func evalPFunction(w *World, plan [][]CNF, l Literal,
	min, max []Entity, xn X, lvl int, out chan eb) {
	vs := make([]Value, len(l.ps))
	vlvl := len(plan[int(l.onVar)]) - 1
	in := resolveVariable(w, plan, min, max, l.onVar, vlvl)
	for e := range in {
		fmt.Printf("Found %v\n", e)
		for i, p := range l.ps {
			vs[i] = w.Component(p.Cid).Get(e.e[p.Idx])
		}
		if l.pfn(e.e, vs) {
			out <- e
		}
	}
}

func evalProduct(w *World, plan [][]CNF, l Literal,
	min, max []Entity, xn X, lvl int, out chan eb) {
	var fn func(xs []X, idx int) chan eb

	fn = func(xs []X, idx int) chan eb {
		out := make(chan eb, 1)
		plvl := len(plan[idx]) - 1
		go func() {
			defer close(out)
			if idx < 0 {
				return
			}
			switch idx {
			case 0:
				for x1 := range resolveVariable(
					w, plan, min, max, xs[idx], plvl) {
					out <- x1
				}
			default:
				for x1 := range fn(xs, idx-1) {
					for x2 := range resolveVariable(
						w, plan, min, max, xs[idx], plvl) {
						es := make([]Entity, len(x1.e)+len(x2.e))
						copy(es, append(x1.e, x2.e...))
						out <- eb{es}
					}
				}
			}
		}()
		return out
	}
	for e := range fn(l.xs, len(l.xs)-1) {
		out <- e
	}
}
func chNot(out, yes, not chan eb) {
	y, yopen := <-yes
	n, nopen := <-not
	for yopen || nopen {
		switch {
		case !nopen:
			out <- y
			y, yopen = <-yes
		case !yopen:
			n, nopen = <-not
		case y.Less(n):
			out <- y
			y, yopen = <-yes
		case n.Less(y):
			n, nopen = <-not
		default:
			y, yopen = <-yes
			n, nopen = <-not
		}
	}
}
func chAnd(out, out1, out2 chan eb) {
	o1, o1open := <-out1
	o2, o2open := <-out2
	for o1open || o2open {
		switch {
		case !o2open:
			o1, o1open = <-out1
		case !o1open:
			o2, o2open = <-out2
		case o1.Less(o2):
			o1, o1open = <-out1
		case o2.Less(o1):
			o2, o2open = <-out2
		default:
			out <- o1
			o1, o1open = <-out1
			o2, o2open = <-out2
		}
	}
}
func chOr(out, out1, out2 chan eb) {
	o1, o1open := <-out1
	o2, o2open := <-out2
	for o1open || o2open {
		switch {
		case !o2open:
			out <- o1
			o1, o1open = <-out1
		case !o1open:
			out <- o2
			o2, o2open = <-out2
		case o1.Less(o2):
			out <- o1
			o1, o1open = <-out1
		case o2.Less(o1):
			out <- o2
			o2, o2open = <-out2
		default:
			out <- o1
			o1, o1open = <-out1
			o2, o2open = <-out2
		}
	}
}
func appendEntity(slice []Entity, data ...Entity) []Entity {
	m := len(slice)
	n := m + len(data)
	if n > cap(slice) {
		newSlice := make([]Entity, (n+1)*2)
		copy(newSlice, slice)
		slice = newSlice
	}
	slice = slice[0:n]
	copy(slice[m:n], data)
	return slice
}
func appendEntitySlice(slice [][]Entity, data ...[]Entity) [][]Entity {
	m := len(slice)
	n := m + len(data)
	if n > cap(slice) {
		newSlice := make([][]Entity, (n+1)*2)
		copy(newSlice, slice)
		slice = newSlice
	}
	slice = slice[0:n]
	copy(slice[m:n], data)
	return slice
}
func appendValueSlice(slice [][]Value, data ...[]Value) [][]Value {
	m := len(slice)
	n := m + len(data)
	if n > cap(slice) {
		newSlice := make([][]Value, (n+1)*2)
		copy(newSlice, slice)
		slice = newSlice
	}
	slice = slice[0:n]
	copy(slice[m:n], data)
	return slice
}
