package ecs

import (
	"testing"
	"time"
)

func TestResolveDomain(t *testing.T) {
	const (
		PLAYER = iota
		PLAYER1
		JOYSTICK
	)
	var w World
	w.RegisterComponents(
		Component{PLAYER, new(FlagStorage)},
		Component{PLAYER1, new(FlagStorage)},
		Component{JOYSTICK, new(FlagStorage)},
	)
	var b NFBuilder
	cnfdomain := b.CNF(
		b.Clause(b.Has(X(0), PLAYER)),
		b.Clause(b.Has(X(1), JOYSTICK)),
		b.Clause(b.Not(b.Has(X(2), JOYSTICK))),
	)
	plan, min, max, err := resolveDomain(&w, cnfdomain)
	if err != nil {
		t.Errorf("error %v has happened", err)
	}
	if len(min) != 3 {
		t.Errorf("Expected min length=3. Got %v", len(min))
	}
	if len(max) != 3 {
		t.Errorf("Expected max length=3. Got %v", len(max))
	}
	for x := range plan {
		for a, cnf := range plan[x] {
			t.Logf("X_%v A_%v %#v", x, a, cnf)
		}
	}
}
func TestLiterals(t *testing.T) {
	const (
		PLAYER = iota
		PLAYER1
		PLAYER2
		JOYSTICK1
		JOYSTICK2
		BULLET
	)
	var w World
	w.RegisterComponents(
		Component{PLAYER, new(FlagStorage)},
		Component{PLAYER1, new(FlagStorage)},
		Component{PLAYER2, new(FlagStorage)},
		Component{JOYSTICK1, new(FlagStorage)},
		Component{JOYSTICK2, new(FlagStorage)},
		Component{BULLET, new(FlagStorage)},
	)
	p1, err := w.MakeEntity(
		ComponentValue{ID: PLAYER},
		ComponentValue{ID: PLAYER1},
		ComponentValue{ID: JOYSTICK1},
	)
	if err != nil {
		t.Errorf("%v", err)
	}
	p2, err := w.MakeEntity(
		ComponentValue{ID: PLAYER},
		ComponentValue{ID: PLAYER2},
		ComponentValue{ID: JOYSTICK2},
	)
	if err != nil {
		t.Errorf("%v", err)
	}
	var bullets [][]Entity
	numBullets := 10000
	pbs := make([][]Entity, numBullets*2)
	for i := 0; i < numBullets; i++ {
		b, err := w.MakeEntity(
			ComponentValue{ID: BULLET},
		)
		if err != nil {
			t.Errorf("%v", err)
		}
		bullets = appendEntitySlice(bullets, []Entity{b})
		pbs[i] = []Entity{p1, b}
		pbs[i+numBullets] = []Entity{p2, b}
	}

	var b NFBuilder
	cnfP1a := b.CNF(b.Clause(b.Has(X(0), PLAYER1)), b.Clause(b.Has(X(0), JOYSTICK1)))
	cnfP2a := b.CNF(b.Clause(b.Has(X(0), PLAYER2)), b.Clause(b.Has(X(0), JOYSTICK2)))
	cnfP2b := b.CNF(b.Clause(b.Has(X(0), PLAYER)), b.Clause(b.Not(b.Has(X(0), PLAYER1))))
	cnfPXa := b.CNF(b.Clause(b.Has(X(0), PLAYER)), b.Clause(b.Not(b.Has(X(0), PLAYER1))), b.Clause(b.Not(b.Has(X(0), JOYSTICK2))))
	cnfPab := b.CNF(b.Clause(b.Has(X(0), PLAYER)))
	cnfPa2 := b.CNF(b.Clause(b.Has(X(0), PLAYER1), b.Has(X(0), PLAYER2)))
	cnfBua := b.CNF(b.Clause(b.Has(X(0), BULLET)))
	cnfPNa := b.CNF(b.Clause(b.Not(b.Has(X(0), PLAYER))))
	cnfPro := b.CNF(b.Clause(b.Has(X(0), PLAYER)), b.Clause(b.Has(X(1), BULLET)), b.Clause(b.Product(X(2), X(0), X(1))))
	cnfPr1 := b.CNF(b.Clause(b.Has(X(0), PLAYER1)), b.Clause(b.Has(X(1), BULLET)), b.Clause(b.Product(X(2), X(0), X(1))))
	pfunc := b.CNF(b.Clause(b.Has(X(0), PLAYER)),
		b.Clause(b.Has(X(1), BULLET)),
		b.Clause(b.Product(X(2), X(0), X(1))),
		b.Clause(b.PFunction(X(3),
			[]Projection{{0, PLAYER}, {0, PLAYER1}, {0, PLAYER2}},
			X(2),
			func(es []Entity, vs []Value) bool {
				if es[0]+es[1] != 20 {
					return false
				}
				t.Logf("P %v, P1 %v, P2 %v", vs[0], vs[1], vs[2])
				return true
			})),
	)

	tests := []struct {
		cnf CNF
		ids [][]Entity
	}{
		{cnf: cnfP1a, ids: [][]Entity{{p1}}},
		{cnf: b.CNF(b.Clause(b.CNFPred(X(0), cnfP1a))), ids: [][]Entity{{p1}}},
		{cnf: cnfP2a, ids: [][]Entity{{p2}}},
		{cnf: b.CNF(b.Clause(b.CNFPred(X(0), cnfP2a))), ids: [][]Entity{{p2}}},
		{cnf: b.CNF(b.Clause(b.CNFPred(X(0), cnfP2a), b.CNFPred(X(0), cnfP1a))), ids: [][]Entity{{p1}, {p2}}},
		{cnf: b.CNF(b.Clause(b.CNFPred(X(0), cnfP2a)), b.Clause(b.CNFPred(X(0), cnfP1a))), ids: [][]Entity{}},
		{cnf: cnfP2b, ids: [][]Entity{{p2}}},
		{cnf: cnfPXa, ids: [][]Entity{}},
		{cnf: cnfPab, ids: [][]Entity{{p1}, {p2}}},
		{cnf: cnfPa2, ids: [][]Entity{{p1}, {p2}}},
		{cnf: cnfBua, ids: bullets},
		{cnf: cnfPNa, ids: bullets},
		{cnf: cnfPro, ids: pbs},
		{cnf: cnfPr1, ids: pbs[:numBullets]},
		{cnf: b.CNF(b.Clause(b.CNFPred(X(0), cnfPro))), ids: pbs},
		{cnf: pfunc, ids: [][]Entity{{1, 19}, {2, 18}}},
	}
	for i, test := range tests {
		start := time.Now()
		ids, err := test.cnf.Fetch(&w)
		if err != nil {
			t.Errorf("expect no errors but got %v", err)
		}
		elapsed := time.Since(start)
		if err != nil {
			t.Errorf("test %v:expect no error,got %v.", i, err)
		}
		if len(ids) != len(test.ids) {
			t.Logf("test %v: %#v", i, ids)
			t.Errorf("test %v:expect length %v, got %v", i, len(test.ids), ids)
		}
		total := 0
		for j, row := range ids {
			total++
			for k, id := range row {
				if id != test.ids[j][k] {
					t.Errorf("test %v:expect  %v, got %v", i, test.ids[j][k], id)
				}
			}
		}
		t.Logf("test %v\n    elapsed: %v\n   elements: %v", i, elapsed, total)
	}
}
