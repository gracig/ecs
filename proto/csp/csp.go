package csp

type Item interface{ LessThan(other Item) bool }
type Interval struct{ Min, Max Item }
type Items []Item
type ItemsStream chan Items
type Domain struct {
	Interval func(...Var) Interval
	Stream   func(Interval) ItemsStream
}
type Var struct {
	Name string
	Domain
}
type Constraint func(vs ...Var) Domain
type CSP struct{}

func (c *CSP) Var(domain Domain) *Var        { return nil }
func (c *CSP) Constraint(Constraint, ...Var) {}
func (c CSP) Solve()                         {}

/*
func teste() {
	var csp CSP
	p := csp.Var(Domain{})
	b := csp.Var(Domain{})
	csp.Constraint(WithComponents(PLAYER, SHAPE), p)
	csp.Constraint(WithoutComponents(PLAYER1), p)
	csp.Constraint(WithComponents(ENEMY, SHAPE), b)
	csp.Constraint(IDSumEquals(20), p, b)
}
*/
