package finder

import (
	"sync"

	"bitbucket.org/gracig/ecs"
)

//ECM represents an entity
//It allows to add the LessThan Method to it
type ECM struct {
	e  ecs.Entity
	cm ComponentMap
}

type ComponentMap map[ecs.ComponentID]ecs.Value

//LessThan transforms ECM into an Item
func (e ECM) LessThan(other Item) bool {
	return e.e < other.(ECM).e
}

//Merge transforms ECM into an Item
func (e ECM) Merge(other Item) Item {
	for k, v := range other.(ECM).cm {
		e.cm[k] = v
		delete(other.(ECM).cm, k)
	}
	other.(ECM).Release()
	return e
}

//Release transforms ECM into an Item
func (e ECM) Release() {
	for k := range e.cm {
		delete(e.cm, k)
	}
	cmPool.Put(e.cm)
	e.cm = nil
}

func (e ECM) Copy() Item {
	cm := cmPool.Get().(ComponentMap)
	for k, v := range e.cm {
		cm[k] = v
	}
	return ECM{e: e.e, cm: cm}
}

var cmPool = sync.Pool{
	New: func() interface{} {
		return make(ComponentMap)
	},
}

func ApplyECSLogic(fn func(ecm ...ECM) bool) func(Items) bool {
	return func(ts Items) bool {
		ecms := make([]ECM, len(ts))
		for i := 0; i < len(ts); i++ {
			ecms[i] = ts[i].(ECM)
		}
		return fn(ecms...)
	}
}

//WorldDomain generate every entity inside a world
func WorldDomain(w *ecs.World) Domain {
	var gs []Domain
	for _, c := range w.Components() {
		gs = append(gs, ComponentDomain(w, c.ID))
	}
	return Or(gs...)
}

//ComponentDomain create a Domain that represents the entities
//inside a component set identified by a ComponentID
func ComponentDomain(w *ecs.World, id ecs.ComponentID) Domain {
	return Domain{
		Interval: func(ts ...Items) Interval {
			min, max := w.ComponentInterval(id)
			return Interval{ECM{e: min}, ECM{e: max}}
		},
		Stream: func(itvl Interval, ts ...Items) ItemStream {
			out := make(ItemStream, 1)
			go func() {
				defer close(out)
				min := ecs.Entity(itvl.Min.(ECM).e)
				max := ecs.Entity(itvl.Max.(ECM).e)
				w.ComponentAscendInterval(id, min, max, func(e ecs.Entity) bool {
					cm := cmPool.Get().(ComponentMap)
					cm[id] = w.Component(id).Get(e)
					out <- append(Items{}, ECM{
						e:  e,
						cm: cm,
					})
					return true
				})
			}()
			return out
		},
	}
}

//EntitySumEquals
func EntitySumEquals(w *ecs.World) func(ecs.Entity, Domain) Domain {
	return func(sum ecs.Entity, d Domain) Domain {
		return Domain{
			Interval: func(tss ...Items) (itvl Interval) {
				itvl = d.Interval(tss...)
				if len(tss) > 0 {
					values := make([]ecs.Entity, 0, len(tss))
					for _, ts := range tss {
						for _, t := range ts {
							myid := sum - t.(ECM).e
							if myid >= itvl.Min.(ECM).e && myid <= itvl.Max.(ECM).e {
								values = append(values, myid)
							}
						}
					}
					if len(values) > 0 {
						itvl.Min = ECM{e: values[len(values)-1]}
						itvl.Max = ECM{e: values[0]}
					} else {
						itvl.Max = ECM{e: 0}
					}
				}
				return
			},
			Stream: d.Stream,
		}
	}
}
