package finder

import (
	"bitbucket.org/gracig/ecs"
	"testing"
	"time"
)

func TestFinder(t *testing.T) {
	const (
		PLAYER = iota
		PLAYER1
		PLAYER2
		JOYSTICK1
		JOYSTICK2
		BULLET
		ENEMY
	)
	var w ecs.World
	w.RegisterComponents(
		ecs.Component{PLAYER, new(ecs.FlagStorage)},
		ecs.Component{PLAYER1, new(ecs.FlagStorage)},
		ecs.Component{PLAYER2, new(ecs.FlagStorage)},
		ecs.Component{JOYSTICK1, new(ecs.FlagStorage)},
		ecs.Component{JOYSTICK2, new(ecs.FlagStorage)},
		ecs.Component{BULLET, new(ecs.FlagStorage)},
		ecs.Component{ENEMY, new(ecs.FlagStorage)},
	)
	p1, err := w.MakeEntity(
		ecs.ComponentValue{ID: PLAYER},
		ecs.ComponentValue{ID: PLAYER1},
		ecs.ComponentValue{ID: JOYSTICK1},
	)
	if err != nil {
		t.Errorf("%v", err)
	}
	p2, err := w.MakeEntity(
		ecs.ComponentValue{ID: PLAYER},
		ecs.ComponentValue{ID: PLAYER2},
		ecs.ComponentValue{ID: JOYSTICK2},
	)
	if err != nil {
		t.Errorf("%v", err)
	}
	var bullets [][]ecs.Entity
	numBullets := 4000
	pbs := make([][]ecs.Entity, numBullets*2)
	for i := 0; i < numBullets; i++ {
		b, err := w.MakeEntity(
			ecs.ComponentValue{ID: BULLET},
			ecs.ComponentValue{ID: ENEMY},
		)
		if err != nil {
			t.Errorf("%v", err)
		}
		bullets = appendEntitySlice(bullets, []ecs.Entity{b})
		pbs[i] = []ecs.Entity{p1, b}
		pbs[i+numBullets] = []ecs.Entity{p2, b}
	}

	all := WorldDomain(&w)
	gPlayer := ComponentDomain(&w, PLAYER)
	gPlayer1 := ComponentDomain(&w, PLAYER1)
	gPlayer2 := ComponentDomain(&w, PLAYER2)
	gJoy1 := ComponentDomain(&w, JOYSTICK1)
	gJoy2 := ComponentDomain(&w, JOYSTICK2)
	gBullet := ComponentDomain(&w, BULLET)
	gEnemy := ComponentDomain(&w, ENEMY)
	gEntitySumEquals := EntitySumEquals(&w)

	gP1a := And(gPlayer1, gJoy1)
	gP2a := And(gPlayer2, gJoy2)
	gP2b := Not(gPlayer, gPlayer1)
	gPXa := Not(gPlayer, Or(gPlayer1, gJoy2))
	gPab := gPlayer
	gPa2 := Or(gPlayer1, gPlayer2)
	gBua := gBullet
	gPNa := Not(all, gPlayer)
	gPro := Product(gPlayer, gBullet)
	gPr1 := Product(gPlayer1, gBullet)
	apply := ApplyECSLogic(func(es ...ECM) bool {
		p, b := es[0], es[1]
		if p.e+b.e != 20 {
			return false
		} else {
			t.Logf("P id:%v value:%#v, Bullet id:%v value:%#v",
				p.e,
				p.cm,
				b.e,
				b.cm,
			)
		}
		return true
	})
	gPr2 := System(apply, Product(Or(gPlayer, gPlayer1, gPlayer2), gBullet))
	gPr3 := Product(Or(gPlayer, gPlayer1, gPlayer2), gEntitySumEquals(20, And(gEnemy, gBullet)))
	gPr4 := Product(Or(gPlayer, gPlayer1, gPlayer2), gEntitySumEquals(5000, And(gEnemy, gBullet)))
	gPr5 := Product(Or(gPlayer, gPlayer1, gPlayer2), gEntitySumEquals(20, And(gEnemy, gBullet)))
	gPr6 := Product(Or(gPlayer, gPlayer1, gPlayer2), gEntitySumEquals(21, And(gEnemy, gBullet)))
	gPr7 := Product(Or(gPlayer, gPlayer1, gPlayer2), gEntitySumEquals(22, And(gEnemy, gBullet)))

	tests := []struct {
		g   Domain
		ids [][]ecs.Entity
	}{
		{g: gP1a, ids: [][]ecs.Entity{{p1}}},
		{g: And(gP1a), ids: [][]ecs.Entity{{p1}}},
		{g: gP2a, ids: [][]ecs.Entity{{p2}}},
		{g: Or(gP2a), ids: [][]ecs.Entity{{p2}}},
		{g: Or(gP2a, gP1a), ids: [][]ecs.Entity{{p1}, {p2}}},
		{g: And(gP2a, gP1a), ids: [][]ecs.Entity{}},
		{g: gP2b, ids: [][]ecs.Entity{{p2}}},
		{g: gPXa, ids: [][]ecs.Entity{}},
		{g: gPab, ids: [][]ecs.Entity{{p1}, {p2}}},
		{g: gPa2, ids: [][]ecs.Entity{{p1}, {p2}}},
		{g: gBua, ids: bullets},
		{g: gPNa, ids: bullets},
		{g: gPro, ids: pbs},
		{g: gPr1, ids: pbs[:numBullets]},
		{g: Or(gPro), ids: pbs},
		{g: gPr2, ids: [][]ecs.Entity{{1, 19}, {2, 18}}},
		{g: gPr3, ids: [][]ecs.Entity{{1, 19}, {2, 18}}},
		{g: gPr4, ids: [][]ecs.Entity{}},
		{g: gPr5, ids: [][]ecs.Entity{{1, 19}, {2, 18}}},
		{g: gPr6, ids: [][]ecs.Entity{{1, 20}, {2, 19}}},
		{g: gPr7, ids: [][]ecs.Entity{{1, 21}, {2, 20}}},
	}
	for i, test := range tests {
		start := time.Now()
		ids := test.g.Fetch()
		elapsed := time.Since(start)
		if err != nil {
			t.Errorf("expected no errors but got %v", err)
		}
		if err != nil {
			t.Errorf("test %v:expected no error,got %v.", i, err)
		}
		if len(ids) != len(test.ids) {
			t.Logf("test %v: %#v", i, ids)
			t.Errorf("test %v:expected length %v, got %v", i, len(test.ids), ids)
		}
		total := 0
		for j, row := range ids {
			total++
			for k, id := range row {
				if ecs.Entity(id.(ECM).e) != test.ids[j][k] {
					t.Errorf("test %v:expected  %v, got %v", i, test.ids[j][k], id)
				}
			}
		}
		t.Logf("test %v\n    elapsed: %v\n   elements: %v", i, elapsed, total)
	}
}

func appendEntitySlice(slice [][]ecs.Entity, data ...[]ecs.Entity) [][]ecs.Entity {
	m := len(slice)
	n := m + len(data)
	if n > cap(slice) {
		newSlice := make([][]ecs.Entity, (n+1)*2)
		copy(newSlice, slice)
		slice = newSlice
	}
	slice = slice[0:n]
	copy(slice[m:n], data)
	return slice
}
