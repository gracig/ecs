package finder

//Item is anything that comes out from a Domain in ascend order
type Item interface {
	LessThan(other Item) bool
	Merge(other Item) Item
	Copy() Item
	Release()
}

//Items is a list of Item
type Items []Item

//ItemStream is a channel of Items
type ItemStream chan Items

//Apply function
type Apply func(Items) bool

//Interval is the min max interval of things inside a generator
type Interval struct{ Min, Max Item }

//Domain is a way to access Items through the use of an ItemStream constrained by Interval
type Domain struct {
	Interval    func(...Items) Interval
	Satisfiable func(...Interval) bool
	Stream      func(Interval, ...Items) ItemStream
}

//Fetch calls the stream function and populates a list of Items
func (g Domain) Fetch() (things []Items) {
	for t := range g.Stream(g.Interval()) {
		things = AppendItems(things, t)
	}
	return
}

//Cycle traverse all elements and increment counter
func (g Domain) Cycle() (counter int) {
	for ts := range g.Stream(g.Interval()) {
		counter++
		ts.Release()
	}
	return
}

//AppendItems append data (a list of Items) to slice (another List of Items)
func AppendItems(slice []Items, data ...Items) []Items {
	m := len(slice)
	n := m + len(data)
	if n > cap(slice) {
		newSlice := make([]Items, (n+1)*2)
		copy(newSlice, slice)
		slice = newSlice
	}
	slice = slice[0:n]
	copy(slice[m:n], data)
	return slice
}

//LessThan operates on Item LessThan
func (ts Items) LessThan(ots Items) bool {
	if !ts[0].LessThan(ots[0]) {
		return false
	}
	if len(ts) > 1 && len(ots) > 1 {
		return ts[1:].LessThan(ots[1:])
	}
	return true
}

//Merge operates on Item Merge
func (ts Items) Merge(ots Items) Items {
	for i := range ts {
		if len(ots) <= i {
			return ts
		}
		ts[i].Merge(ots[i])
	}
	return ts
}

//Copy operates on Item Merge
func (ts Items) Copy() Items {
	//cts := ItemsPool.Get().(Items)
	cts := make(Items, 0, len(ts))
	for _, t := range ts {
		cts = append(cts, t.Copy())
	}
	return cts
}

//Release operates on Item Release
func (ts Items) Release() {
	for i := len(ts) - 1; i >= 0; i-- {
		ts[i].Release()
	}
	//ItemsPool.Put(ts[0:0])
}

//sendItems applies a function before send over channel
func send(out ItemStream, ts Items, apply Apply) {
	if apply != nil {
		if !apply(ts) {
			ts.Release()
			return
		}
	}
	out <- ts
	return
}

//Or merges all input generators using the Or Logic Gate
func Or(xs ...Domain) Domain {
	return Domain{
		Interval: func(ts ...Items) (d Interval) {
			for i := range xs {
				xd := xs[i].Interval(ts...)
				switch {
				case i == 0:
					d = xd
				default:
					if xd.Min.LessThan(d.Min) {
						d.Min = xd.Min
					}
					if d.Max.LessThan(xd.Max) {
						d.Max = xd.Max
					}
				}
			}
			return
		},
		Stream: func(interval Interval, ts ...Items) ItemStream {
			out := make(ItemStream, 1)
			go func() {
				defer close(out)
				switch len(xs) {
				case 0:
					return
				case 1:
					for x := range xs[0].Stream(interval) {
						send(out, x, nil)
					}
				case 2:
					xs0, xs1 := xs[0].Stream(interval), xs[1].Stream(interval)
					x0, xopen := <-xs0
					x1, yopen := <-xs1
					for xopen || yopen {
						switch {
						case !yopen:
							send(out, x0, nil)
							x0, xopen = <-xs0
						case !xopen:
							send(out, x1, nil)
							x1, yopen = <-xs1
						case x0.LessThan(x1):
							send(out, x0, nil)
							x0, xopen = <-xs0
						case x1.LessThan(x0):
							send(out, x1, nil)
							x1, yopen = <-xs1
						default:
							send(out, x0.Merge(x1), nil)
							x0, xopen = <-xs0
							x1, yopen = <-xs1
						}
					}
				default:
					for x := range Or(xs[0], Or(xs[1:]...)).Stream(interval) {
						send(out, x, nil)
					}
				}
			}()
			return out
		},
	}
}

//And merges all input generators using the And Logic Gate
func And(xs ...Domain) Domain {
	return Domain{
		Interval: func(ts ...Items) (d Interval) {
			for i := range xs {
				xd := xs[i].Interval(ts...)
				switch {
				case i == 0:
					d = xd
				default:
					if d.Min.LessThan(xd.Min) {
						d.Min = xd.Min
					}
					if xd.Max.LessThan(d.Max) {
						d.Max = xd.Max
					}
				}
			}
			return
		},
		Satisfiable: func(intervals ...Interval) bool {
			return true
		},
		Stream: func(interval Interval, ts ...Items) ItemStream {
			out := make(ItemStream, 1)
			go func() {
				defer close(out)
				switch len(xs) {
				case 0:
					return
				case 1:
					for x := range xs[0].Stream(interval) {
						send(out, x, nil)
					}
				case 2:
					xs0, xs1 := xs[0].Stream(interval), xs[1].Stream(interval)
					x0, xopen := <-xs0
					x1, yopen := <-xs1
					for xopen || yopen {
						switch {
						case !yopen:
							x0, xopen = <-xs0
						case !xopen:
							x1, yopen = <-xs1
						case x0.LessThan(x1):
							x0, xopen = <-xs0
						case x1.LessThan(x0):
							x1, yopen = <-xs1
						default:
							send(out, x0.Merge(x1), nil)
							x0, xopen = <-xs0
							x1, yopen = <-xs1
						}
					}
				default:
					for x := range And(xs[0], And(xs[1:]...)).Stream(interval) {
						send(out, x, nil)
					}
				}
			}()
			return out
		},
	}
}

//Not merges all input generators using the Not Logic Gate
func Not(yes, not Domain) Domain {
	return Domain{
		Interval: func(ts ...Items) (d Interval) {
			d = yes.Interval(ts...)
			return
		},
		Stream: func(interval Interval, ts ...Items) ItemStream {
			out := make(ItemStream, 1)
			go func() {
				defer close(out)
				xs0, xs1 := yes.Stream(interval), not.Stream(interval)
				x0, xopen := <-xs0
				x1, yopen := <-xs1
				for xopen || yopen {
					switch {
					case !yopen:
						send(out, x0, nil)
						x0, xopen = <-xs0
					case !xopen:
						x1, yopen = <-xs1
					case x0.LessThan(x1):
						send(out, x1, nil)
						x0, xopen = <-xs0
					case x1.LessThan(x0):
						x1, yopen = <-xs1
					default:
						x0, xopen = <-xs0
						x1, yopen = <-xs1
					}
				}
			}()
			return out
		},
	}
}

//Product merges all input generators as a cartesian product
func Product(xs ...Domain) Domain {
	return Domain{
		Interval: func(ts ...Items) (d Interval) {
			for i := range xs {
				xd := xs[i].Interval(ts...)
				switch {
				case i == 0:
					d = xd
					return
				}
			}
			return
		},
		Stream: func(interval Interval, ts ...Items) ItemStream {
			out := make(ItemStream, 1)
			go func() {
				defer close(out)
				switch len(xs) {
				case 0:
					return
				case 1:
					for x := range xs[0].Stream(interval, ts...) {
						send(out, x, nil)
					}
				default:
					for a := range Product(xs[0]).Stream(interval) {
						itvl := xs[1].Interval(a)
						if itvl.Max.LessThan(itvl.Min) {
							continue
						}
						for b := range Product(xs[1:]...).Stream(itvl, a) {
							t := make(Items, len(a)+len(b))
							copy(t, append(a.Copy(), b...))
							send(out, t, nil)
						}
					}
				}
			}()
			return out
		},
	}
}

//System applies logic to a specific Domain
func System(apply Apply, x Domain) Domain {
	return Domain{
		Interval: func(ts ...Items) (d Interval) {
			d = x.Interval(ts...)
			return
		},
		Stream: func(interval Interval, ts ...Items) ItemStream {
			out := make(ItemStream, 1)
			go func() {
				defer close(out)
				for x := range x.Stream(interval, ts...) {
					send(out, x, apply)
				}
			}()
			return out
		},
	}
}
